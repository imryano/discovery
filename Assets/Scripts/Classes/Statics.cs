﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Statics : MonoBehaviour
{
    public static Dictionary<string, Skill> HunterSkills = new Dictionary<string, Skill>
    {
        { "Torch", new Skill{ CurrentCD = 0, Cooldown = -1, Name = "Torch", ActivateMethod = DefaultActivate, MethodArgs = new List<object> { } } },
        { "Light Fire",  new Skill{ CurrentCD = 0, Cooldown = 10, Name = "Light Fire", ActivateMethod = CreateFire, MethodArgs = new List<object> { } } },
    };

    public static Dictionary<string, Hunter> HunterTypes = new Dictionary<string, Hunter>
    {
        {
           "Torch",
           new Hunter {
                Name = "Torch",
                ViewGrid = new ViewGridRow[] {
                    new ViewGridRow { Before = 0, After = 0 },
                    new ViewGridRow { Before = 1, After = 1 },
                    new ViewGridRow { Before = 2, After = 2 },
                    new ViewGridRow { Before = 3, After = 3 },
                    new ViewGridRow { Before = 2, After = 2 },
                    new ViewGridRow { Before = 1, After = 1 },
                    new ViewGridRow { Before = 0, After = 0 }
                },
                PlayerRow = 4,
                Skills = new Skill[] {
                    HunterSkills["Torch"],
                    HunterSkills["Light Fire"]
                }
            }
        },
         {
           "Gun",
           new Hunter {
                Name = "Gun",
                ViewGrid = new ViewGridRow[] {
                    new ViewGridRow { Before = 0, After = 0 },
                    new ViewGridRow { Before = 1, After = 1 },
                    new ViewGridRow { Before = 2, After = 7 },
                    new ViewGridRow { Before = 1, After = 1 },
                    new ViewGridRow { Before = 0, After = 0 }
                },
                PlayerRow = 3,
                Skills = new Skill[] {
                    HunterSkills["Torch"],
                    HunterSkills["Light Fire"]
                }
            }
        }
    };

    #region Skill Functions

    /// <summary>
    /// The default action for an actuivation. Does nothing and sets the cooldown.
    /// </summary>
    /// <param name="args">
    ///     [0] The cooldown as an integer
    /// </param>
    /// <returns>The cooldown timer</returns>
    public static int DefaultActivate(object[] args)
    {
        int Cooldown = (int)args[0];
        return Cooldown;
    }

    /// <summary>
    /// Instantiates an object in the game
    /// </summary>
    /// <param name="CreateObject">The object to create</param>
    /// <param name="Location">The posiiton to create it</param>
    /// <param name="Rotation">The rotation of the object</param>
    /// <returns>True if successful, False otherwise.</returns>
    public static GameObject CreateObject(Object CreateObject, Vector3 Location, Quaternion Rotation)
    {
        return (GameObject)Instantiate(CreateObject, Location, Rotation);
    }

    /// <summary>
    /// Lights a fire to improve view range
    /// </summary>
    /// <param name="args">
    ///     [1] The player object
    ///     [1] The object to create as a GameObject
    /// </param>
    /// <returns>The cooldown timer</returns>
    public static int CreateFire(object[] args)
    {
        int Cooldown = (int)args[0];
        GameObject PlayerObject = (GameObject)(args[1]);
        Object FireObject = Resources.Load("PlayerFire");
        GameObject CreatedFire = CreateObject(FireObject, new Vector3(PlayerObject.transform.position.x, PlayerObject.transform.position.y - 0.5f, PlayerObject.transform.position.z), Quaternion.identity);

        if (CreatedFire != null)
        {
            CreatedFire.GetComponent<PlayerFire>().Player = PlayerObject;
            return Cooldown;
        } else
        {
            //Failure
            return -1;
        }
    }

    #endregion
}

