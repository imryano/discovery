﻿using UnityEngine;
using System.Collections;

public class ViewGridRow
{
    public int Before;
    public int After;
    public bool Inline = true;
}
