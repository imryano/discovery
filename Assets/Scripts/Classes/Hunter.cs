﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Hunter
{
    public string Name;
    public ViewGridRow[] ViewGrid;
    public Skill[] Skills;
    public int PlayerRow;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

