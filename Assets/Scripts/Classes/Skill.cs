﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;

[System.Serializable]
public class Skill
{
    public string Name;
    public int Cooldown;
    public Func<object[], int> ActivateMethod;
    public List<object> MethodArgs;

    public int CurrentCD = 0;

    public int Activate()
    {
        List<object> args = new List<object> { };
        //First argument is always the cooldown
        args.Add(Cooldown);
        //Add rest of arguments in order
        args.AddRange(MethodArgs);
        CurrentCD = Math.Max(ActivateMethod(args.ToArray()), 0);

        return CurrentCD;
    }
}

