﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewGenerator : MonoBehaviour {
    public GameObject[,] Tiles = new GameObject[200, 200];

    public void ReDrawView(ViewGridRow[] ViewGrid, int ObjectRow, Vector3 FacingDirection)
    {
        for (int c = 0; c < transform.childCount; c++)
        {
            Destroy(transform.GetChild(c).gameObject);
        }

        Object TileObject = Resources.Load("Tile");

        for (int x = -(ObjectRow - 1); x <= ViewGrid.Length - ObjectRow; x++)
        {
            int i = x + (ObjectRow - 1);
            bool Inline = ViewGrid[i].Inline;

            for (int z = -ViewGrid[i].Before; z <= ViewGrid[i].After; z++)
            {
                if (Inline || z != 0)
                {
                    float xTrans = FacingDirection.x != 0 ? FacingDirection.x : 1;
                    float zTrans = FacingDirection.z != 0 ? FacingDirection.z : 1;

                    Vector3 pos = new Vector3();

                    if (FacingDirection.x != 0)
                    {
                        pos = new Vector3(transform.position.x + (z * 2.5f * xTrans), 0, transform.position.z + (x * 2.5f));
                    } else
                    {
                        pos = new Vector3(transform.position.x + (x * 2.5f), 0, transform.position.z + (z * 2.5f * -zTrans));
                    }

                    Tiles[x + 100, z + 100] = (GameObject)Instantiate(TileObject, pos, Quaternion.identity, transform);
                }
            }
        }
    }
}
