﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour {
    public ViewGenerator ViewGen;
    public GameObject Player;

    ViewGridRow[] ViewGrid = new ViewGridRow[] {
                new ViewGridRow { Before = 0, After = 0 },
                new ViewGridRow { Before = 1, After = 1 },
                new ViewGridRow { Before = 0, After = 0 }
            };
    int ObjectRow = 2;

	// Use this for initialization
	void Start () {
        ViewGen.ReDrawView(ViewGrid, ObjectRow, Vector3.zero);
	}
	
	// Update is called once per frame
	void Update () {
		if (((Mathf.Abs(Player.transform.position.x - transform.position.x) + Mathf.Abs(Player.transform.position.z - transform.position.z)) / 2.5f) > 5)
        {
            Destroy(gameObject);
        }
	}
}
