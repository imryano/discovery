﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public Vector3 CurrentDirection = Vector3.forward;
    public ViewGenerator ViewGen;

    [SerializeField]
    private Hunter _hunterType = Statics.HunterTypes["Torch"];
        public Hunter HunterType
    {
        get
        {
            return _hunterType;
        }
        set
        {
            _hunterType = value;

            if (_hunterType != null)
            {
                //Set the GameObject arguemnt for passing through to skill activations
                for (int i = 0; i < _hunterType.Skills.Length; i++)
                {
                    _hunterType.Skills[i].MethodArgs.Insert(0, gameObject);
                }
            }
        }
    }

    public void Start()
    {
        HunterType = Statics.HunterTypes["Torch"];
        ViewGen.ReDrawView(HunterType.ViewGrid, HunterType.PlayerRow, CurrentDirection);
    }

    void Update()
    {
        LookAtMouse();

        if (Input.GetKeyDown("w"))
        {
            Move(0, 2.5f);
        }

        if (Input.GetKeyDown("s"))
        {
            Move(0, -2.5f);
        }

        if (Input.GetKeyDown("a"))
        {
            Move(-2.5f, 0);
        }

        if (Input.GetKeyDown("d"))
        {
            Move(2.5f, 0);
        }

        for (int i = 0; i < _hunterType.Skills.Length; i++)
        {
            if (Input.GetKeyDown((i + 1).ToString()) && _hunterType.Skills[i].CurrentCD == 0)
            {
                _hunterType.Skills[i].Activate();
            }
        } 
    }

    private void Move(float x, float z)
    {
        transform.Translate(x, 0, z);
        for (int i = 0; i < _hunterType.Skills.Length; i++)
        {
            if (_hunterType.Skills[i].CurrentCD > 0)
            {
                _hunterType.Skills[i].CurrentCD -= 1;
            }
        }

        ViewGen.ReDrawView(HunterType.ViewGrid, HunterType.PlayerRow, CurrentDirection);
    }

    private void LookAtMouse()
    {
        float LookX = Camera.main.WorldToScreenPoint(transform.position).x - Input.mousePosition.x;
        float LookZ = Camera.main.WorldToScreenPoint(transform.position).y - Input.mousePosition.y;
        Vector3 PreviousDirection = CurrentDirection;

        if (Mathf.Abs(LookX) > Mathf.Abs(LookZ)) {
            CurrentDirection = Vector3.left * Mathf.Sign(LookX);
        } else
        {
            CurrentDirection = Vector3.forward * Mathf.Sign(LookZ);
        }

        if (PreviousDirection != CurrentDirection)
        {
            ViewGen.ReDrawView(HunterType.ViewGrid, HunterType.PlayerRow, CurrentDirection);
        }
    }
}
